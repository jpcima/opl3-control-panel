
#include "miniwopl.h"
#include "OPLSynth.h"
#include "resource.h"
#include <windows.h>
#include <windowsx.h>
#include <shellapi.h>
#include <tchar.h>
#include <stdio.h>
#include <type_traits>
#include <memory>
#include <iostream>

struct HMIDIOUT_deleter
    { void operator()(HMIDIOUT h) { midiOutClose(h); } };
typedef std::unique_ptr<std::remove_pointer<HMIDIOUT>::type, HMIDIOUT_deleter>
    unique_HMIDIOUT;

HINSTANCE hInstance = nullptr;
HWND hWndMain = nullptr;
HINSTANCE hDllShell32 = nullptr;

BOOL DeviceMatch(const MIDIOUTCAPS *caps)
{
    const TCHAR szHwPrefix[] = _T("YMF262 (OPL3) FM Synthesizer");
    const TCHAR szSwPrefix[] = _T("YMF262 Software Synth");
    return !_tcsncmp(caps->szPname, szHwPrefix, _tcslen(szHwPrefix)) ||
        !_tcsncmp(caps->szPname, szSwPrefix, _tcslen(szSwPrefix));
}

void UpdateDeviceList(HWND ctlList)
{
    UINT uDeviceCount = midiOutGetNumDevs();

    while (ComboBox_GetCount(ctlList) > 0)
        ComboBox_DeleteString(ctlList, 0);

    for (UINT uDeviceId = 0; uDeviceId < uDeviceCount; ++uDeviceId) {
        MIDIOUTCAPS caps;
        if (midiOutGetDevCaps(uDeviceId, &caps, sizeof(caps)) != MMSYSERR_NOERROR)
            continue;

        if (DeviceMatch(&caps)) {
            TCHAR szLabel[256];
            _sntprintf(szLabel, 255, _T("%u: %s"), uDeviceId, caps.szPname);
            szLabel[255] = 0;

            int index = ComboBox_AddString(ctlList, szLabel);
            ComboBox_SetItemData(ctlList, index, uDeviceId);
        }
    }

    ComboBox_SetCurSel(ctlList, 0);
}

MMRESULT DevicePlaySysex(HMIDIOUT hMidiOut, BYTE *pMsg, DWORD dwLength)
{
    MMRESULT mmres;
    MIDIHDR hdr = {};
    hdr.lpData = (char *)pMsg;
    hdr.dwBufferLength = dwLength;
    pMsg[2] = 0x7f; // broadcast id

#if 1
    mmres = midiOutPrepareHeader(hMidiOut, &hdr, sizeof(hdr));
    if (mmres != MMSYSERR_NOERROR)
        return mmres;
    mmres = midiOutLongMsg(hMidiOut, &hdr, sizeof(hdr));
    if (mmres != MMSYSERR_NOERROR)
        return mmres;
    do {
        mmres = midiOutUnprepareHeader(hMidiOut, &hdr, sizeof(hdr));
        if (mmres  == MIDIERR_STILLPLAYING)
            Sleep(1);
    } while (mmres  == MIDIERR_STILLPLAYING);
#endif

    return mmres;
}

BOOL DeviceLoadInstrumentFile(HWND hDlg, UINT uDeviceId, LPCTSTR lpszFileName)
{
    TCHAR szMsgbuf[256];
    szMsgbuf[255] = 0;

    WOPL_FileReader fileReader;
    fileReader.openFile(lpszFileName);

    WOPL_Bank bank;
    if (!LoadBank(fileReader, bank)) {
        MessageBox(hDlg, _T("Could not load the instrument file."), _T("Error"), MB_OK|MB_ICONWARNING);
        return FALSE;
    }

#if 1
    HMIDIOUT hMidiOut;
    if (midiOutOpen(&hMidiOut, uDeviceId, (DWORD_PTR)hDlg, 0, CALLBACK_WINDOW) != MMSYSERR_NOERROR) {
        MessageBox(hDlg, _T("Could not open the MIDI device."), _T("Error"), MB_OK|MB_ICONWARNING);
        return FALSE;
    }
    unique_HMIDIOUT autoCloseMidiOut(hMidiOut);
#endif

    std::vector<BYTE> sysex_message;
    Syx_Writer syxWriter(sysex_message);
    sysex_message.reserve(256);

    auto SendSysex = [&]() -> BOOL {
#if 1
        MMRESULT mmres = DevicePlaySysex(
            hMidiOut, sysex_message.data(), sysex_message.size());
        if (mmres != MMSYSERR_NOERROR) {
            _sntprintf(szMsgbuf, 255, _T("Could not send the MIDI data. (code %lu)"), mmres);
            return FALSE;
        }
#endif
        return TRUE;
    };

    unsigned melo_count = 0;
    unsigned perc_count = 0;

    for (unsigned i = 0; i < 256; ++i) {
        patchStruct patch = {};
        patchMapStruct melmap = {};
        percMapStruct percmap = {};

        melmap.bPreset = i;
        percmap.bPreset = i;

        if (i < 128 && i < bank.inst_melodic.size()) {
            ConvertFromWOPL(i, patch, &melmap, nullptr, bank.inst_melodic[i]);

            sysex_message.clear();
            PatchToSyx(syxWriter, i, patch);
            if (!SendSysex())
                return FALSE;

            sysex_message.clear();
            MelMapToSyx(syxWriter, i, melmap);
            if (!SendSysex())
                return FALSE;

            ++melo_count;
        }
        else if (i >= 128 && (i - 128) < bank.inst_percussive.size()) {
            ConvertFromWOPL(i, patch, nullptr, &percmap, bank.inst_percussive[i - 128]);

            sysex_message.clear();
            PatchToSyx(syxWriter, i, patch);
            if (!SendSysex())
                return FALSE;

            sysex_message.clear();
            PercMapToSyx(syxWriter, i, percmap);
            if (!SendSysex())
                return FALSE;

            ++perc_count;
        }
    }

    _sntprintf(szMsgbuf, 255, _T("Instruments loaded: %u melodic, %u percussive"),
               melo_count, perc_count);
    MessageBox(hDlg, szMsgbuf, _T("Success"), MB_OK|MB_ICONINFORMATION);

    return TRUE;
}

BOOL CALLBACK MainDialogProc(
    HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    HINSTANCE hInstance = ::hInstance;

    switch (message) {
    case WM_INITDIALOG: {
        HWND ctlDevice = GetDlgItem(hDlg, IDC_CHOOSEDEVICE);
        UpdateDeviceList(ctlDevice);
        HWND ctlChooseFile = GetDlgItem(hDlg, IDC_INSCHOOSEFILE);
        SendMessage(ctlChooseFile, BM_SETIMAGE, IMAGE_ICON,
                    (LPARAM)LoadImage(::hDllShell32, MAKEINTRESOURCE(4), IMAGE_ICON, 16, 16, LR_LOADTRANSPARENT));
        break;
    }

    case MM_MOM_OPEN:
        fprintf(stderr, "MM_MOM_OPEN\n");
        fflush(stderr);
        break;
    case MM_MOM_CLOSE:
        fprintf(stderr, "MM_MOM_CLOSE\n");
        fflush(stderr);
        break;
    case MM_MOM_DONE:
        fprintf(stderr, "MM_MOM_DONE\n");
        fflush(stderr);
        break;

    case WM_COMMAND:
        switch (LOWORD(wParam))
        {
        case IDC_INSLOAD: {
            HWND ctlDevice = GetDlgItem(hDlg, IDC_CHOOSEDEVICE);
            int index = ComboBox_GetCurSel(ctlDevice);
            if (index != CB_ERR) {
                HWND ctlEdit = GetDlgItem(hDlg, IDC_INSPATH);
                TCHAR szFilename[MAX_PATH+1] = _T("");
                Edit_GetText(ctlEdit, szFilename, MAX_PATH);
                DeviceLoadInstrumentFile(
                    hDlg, ComboBox_GetItemData(ctlDevice, index), szFilename);
            }
            break;
        }

        case IDC_INSCHOOSEFILE: {
            OPENFILENAME ofn = {};
            TCHAR szFilename[MAX_PATH+1] = _T("");
            ofn.lStructSize = sizeof(ofn);
            ofn.hwndOwner = hDlg;
            ofn.hInstance = hInstance;
            ofn.Flags = OFN_HIDEREADONLY;
            ofn.lpstrFilter = _T("WOPL files (*.wopl)\0*.wopl\0\0");
            ofn.lpstrFile = szFilename;
            ofn.nMaxFile = MAX_PATH;
            if (GetOpenFileName(&ofn)) {
                HWND ctlEdit = GetDlgItem(hDlg, IDC_INSPATH);
                Edit_SetText(ctlEdit, szFilename);
                HWND ctlLoad = GetDlgItem(hDlg, IDC_INSLOAD);
                Button_Enable(ctlLoad, TRUE);
            }
            break;
        }

        case IDOK:
        case IDCANCEL: {
            DestroyWindow(hDlg);
            ::hWndMain = nullptr;
            return true;
            break;
        }
        }
        break;
    }

    return FALSE;
}

int _tmain(int argc, TCHAR *argv[])
{
    HINSTANCE hInstance = ::hInstance = GetModuleHandle(nullptr);
    ::hDllShell32 = LoadLibrary(_T("shell32.dll"));

    HWND hWndMain = ::hWndMain = CreateDialog(
        hInstance,
        MAKEINTRESOURCE(IDD_DIALOG1),
        nullptr,
        &MainDialogProc);
    if (!hWndMain)
        return 1;

    MSG msg;
    while (::hWndMain && GetMessage(&msg, nullptr, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}

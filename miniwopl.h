/*
 * libADLMIDI is a free MIDI to WAV conversion library with OPL3 emulation
 *
 * Original ADLMIDI code: Copyright (c) 2010-2014 Joel Yliluoma <bisqwit@iki.fi>
 * ADLMIDI Library API:   Copyright (c) 2015-2018 Vitaly Novichkov <admin@wohlnet.ru>
 *
 * Library is based on the ADLMIDI, a MIDI player for Linux and Windows with OPL3 emulation:
 * http://iki.fi/bisqwit/source/adlmidi.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include "miniwopl_fileio.h"
#include <vector>
#include <stdint.h>

static const char       *wopl3_magic = "WOPL3-BANK\0";
static const uint16_t   wopl_latest_version = 3;

#define WOPL_INST_SIZE_V2 62
#define WOPL_INST_SIZE_V3 66

struct adldata
{
    uint32_t    modulator_E862, carrier_E862;  // See below
    uint8_t     modulator_40, carrier_40; // KSL/attenuation settings
    uint8_t     feedconn; // Feedback/connection bits for the channel

    int8_t      finetune;
};

struct adlinsdata
{
    enum { Flag_Pseudo4op = 0x01, Flag_NoSound = 0x02 };

    uint16_t    adlno1, adlno2;
    uint8_t     tone;
    uint8_t     flags;
    uint16_t    ms_sound_kon;  // Number of milliseconds it produces sound;
    uint16_t    ms_sound_koff;
    double voice2_fine_tune;
};

enum WOPL_InstrumentFlags
{
    WOPL_Flags_NONE      = 0,
    WOPL_Flag_Enable4OP  = 0x01,
    WOPL_Flag_Pseudo4OP  = 0x02
};

struct WOPL_Inst
{
    bool fourOps;
    char padding[7];
    adlinsdata adlins;
    adldata    op[2];
    uint16_t ms_sound_kon;
    uint16_t ms_sound_koff;
    uint8_t voice2_fine_tune;
};

struct WOPL_Bank
{
    std::vector<WOPL_Inst> inst_melodic;
    std::vector<WOPL_Inst> inst_percussive;
};

class WOPL_FileReader;
bool LoadBank(WOPL_FileReader &fr, WOPL_Bank &bnk);

/*
 * libADLMIDI is a free MIDI to WAV conversion library with OPL3 emulation
 *
 * Original ADLMIDI code: Copyright (c) 2010-2014 Joel Yliluoma <bisqwit@iki.fi>
 * ADLMIDI Library API:   Copyright (c) 2015-2018 Vitaly Novichkov <admin@wohlnet.ru>
 *
 * Library is based on the ADLMIDI, a MIDI player for Linux and Windows with OPL3 emulation:
 * http://iki.fi/bisqwit/source/adlmidi.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if defined(_WIN32)
# include <windows.h>
#endif
#include <cstring>
#include <cstdio>
#include <cstdint>

/**
 * @brief A little class gives able to read filedata from disk and also from a memory segment
 */
class WOPL_FileReader
{
public:
    enum relTo
    {
        SET = 0,
        CUR = 1,
        END = 2
    };

    WOPL_FileReader()
    {
        fp = NULL;
        mp = NULL;
        mp_size = 0;
        mp_tell = 0;
    }
    ~WOPL_FileReader()
    {
        close();
    }

    #if defined(_WIN32)
    void openFile(const WCHAR *widePath)
    {
        fp = _wfopen(widePath, L"rb");
        mp = NULL;
        mp_size = 0;
        mp_tell = 0;
    }
    #endif

    void openFile(const char *path)
    {
        #if !defined(_WIN32) || defined(__WATCOMC__)
        fp = std::fopen(path, "rb");
        #else
        wchar_t widePath[MAX_PATH];
        int size = MultiByteToWideChar(CP_UTF8, 0, path, (int)std::strlen(path), widePath, MAX_PATH);
        widePath[size] = '\0';
        fp = _wfopen(widePath, L"rb");
        #endif
        mp = NULL;
        mp_size = 0;
        mp_tell = 0;
    }

    void openData(const void *mem, size_t lenght)
    {
        fp = NULL;
        mp = mem;
        mp_size = lenght;
        mp_tell = 0;
    }

    void seek(long pos, int rel_to)
    {
        if(fp)
            std::fseek(fp, pos, rel_to);
        else
        {
            switch(rel_to)
            {
            case SET:
                mp_tell = static_cast<size_t>(pos);
                break;

            case END:
                mp_tell = mp_size - static_cast<size_t>(pos);
                break;

            case CUR:
                mp_tell = mp_tell + static_cast<size_t>(pos);
                break;
            }

            if(mp_tell > mp_size)
                mp_tell = mp_size;
        }
    }

    inline void seeku(uint64_t pos, int rel_to)
    {
        seek(static_cast<long>(pos), rel_to);
    }

    size_t read(void *buf, size_t num, size_t size)
    {
        if(fp)
            return std::fread(buf, num, size, fp);
        else
        {
            size_t pos = 0;
            size_t maxSize = static_cast<size_t>(size * num);

            while((pos < maxSize) && (mp_tell < mp_size))
            {
                reinterpret_cast<unsigned char *>(buf)[pos] = reinterpret_cast<unsigned const char *>(mp)[mp_tell];
                mp_tell++;
                pos++;
            }

            return pos;
        }
    }

    int getc()
    {
        if(fp)
            return std::getc(fp);
        else
        {
            if(mp_tell >= mp_size) return -1;
            int x = reinterpret_cast<unsigned const char *>(mp)[mp_tell];
            mp_tell++;
            return x;
        }
    }

    size_t tell()
    {
        if(fp)
            return static_cast<size_t>(std::ftell(fp));
        else
            return mp_tell;
    }

    void close()
    {
        if(fp) std::fclose(fp);

        fp = NULL;
        mp = NULL;
        mp_size = 0;
        mp_tell = 0;
    }

    bool isValid()
    {
        return (fp) || (mp);
    }

    bool eof()
    {
        if(fp)
            return std::feof(fp);
        else
            return mp_tell >= mp_size;
    }
    std::FILE   *fp;
    const void  *mp;
    size_t      mp_size;
    size_t      mp_tell;
};

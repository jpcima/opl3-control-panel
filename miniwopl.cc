/*
 * libADLMIDI is a free MIDI to WAV conversion library with OPL3 emulation
 *
 * Original ADLMIDI code: Copyright (c) 2010-2014 Joel Yliluoma <bisqwit@iki.fi>
 * ADLMIDI Library API:   Copyright (c) 2015-2018 Vitaly Novichkov <admin@wohlnet.ru>
 *
 * Library is based on the ADLMIDI, a MIDI player for Linux and Windows with OPL3 emulation:
 * http://iki.fi/bisqwit/source/adlmidi.html
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "miniwopl.h"
#include <cstring>

#define ERR(x) fprintf(stderr, "%s\n", (x))

/* WOPL-needed misc functions */
static uint16_t toUint16LE(const uint8_t *arr)
{
    uint16_t num = arr[0];
    num |= ((arr[1] << 8) & 0xFF00);
    return num;
}

static uint16_t toUint16BE(const uint8_t *arr)
{
    uint16_t num = arr[1];
    num |= ((arr[0] << 8) & 0xFF00);
    return num;
}

static int16_t toSint16BE(const uint8_t *arr)
{
    int16_t num = *reinterpret_cast<const int8_t *>(&arr[0]);
    num *= 1 << 8;
    num |= arr[1];
    return num;
}

static bool readInstrument(WOPL_FileReader &file, WOPL_Inst &ins, uint16_t &version, bool isPercussion)
{
    uint8_t idata[WOPL_INST_SIZE_V3];
    if(version >= 3)
    {
        if(file.read(idata, 1, WOPL_INST_SIZE_V3) != WOPL_INST_SIZE_V3)
            return false;
    }
    else
    {
        if(file.read(idata, 1, WOPL_INST_SIZE_V2) != WOPL_INST_SIZE_V2)
            return false;
    }

    //strncpy(ins.name, char_p(idata), 32);
    ins.op[0].finetune = (int8_t)toSint16BE(idata + 32);
    ins.op[1].finetune = (int8_t)toSint16BE(idata + 34);
    //ins.velocity_offset = int8_t(idata[36]);

    ins.adlins.voice2_fine_tune = 0.0;
    int8_t voice2_fine_tune = int8_t(idata[37]);
    ins.voice2_fine_tune = voice2_fine_tune;
    if(voice2_fine_tune != 0)
    {
        if(voice2_fine_tune == 1)
            ins.adlins.voice2_fine_tune = 0.000025;
        else if(voice2_fine_tune == -1)
            ins.adlins.voice2_fine_tune = -0.000025;
        else
            ins.adlins.voice2_fine_tune = ((voice2_fine_tune * 15.625) / 1000.0);
    }

    ins.adlins.tone = isPercussion ? idata[38] : 0;

    uint8_t flags       = idata[39];
    ins.adlins.flags = (flags & WOPL_Flag_Enable4OP) && (flags & WOPL_Flag_Pseudo4OP) ? adlinsdata::Flag_Pseudo4op : 0;
    ins.fourOps      = (flags & WOPL_Flag_Enable4OP) || (flags & WOPL_Flag_Pseudo4OP);

    ins.op[0].feedconn = (idata[40]);
    ins.op[1].feedconn = (idata[41]);

    for(size_t op = 0, slt = 0; op < 4; op++, slt++)
    {
        size_t off = 42 + size_t(op) * 5;
        //        ins.setAVEKM(op,    idata[off + 0]);//AVEKM
        //        ins.setAtDec(op,    idata[off + 2]);//AtDec
        //        ins.setSusRel(op,   idata[off + 3]);//SusRel
        //        ins.setWaveForm(op, idata[off + 4]);//WaveForm
        //        ins.setKSLL(op,     idata[off + 1]);//KSLL
        ins.op[slt].carrier_E862 =
            ((static_cast<uint32_t>(idata[off + 4]) << 24) & 0xFF000000)    //WaveForm
            | ((static_cast<uint32_t>(idata[off + 3]) << 16) & 0x00FF0000)  //SusRel
            | ((static_cast<uint32_t>(idata[off + 2]) << 8) & 0x0000FF00)   //AtDec
            | ((static_cast<uint32_t>(idata[off + 0]) << 0) & 0x000000FF);  //AVEKM
        ins.op[slt].carrier_40 = idata[off + 1];//KSLL

        op++;
        off = 42 + size_t(op) * 5;
        ins.op[slt].modulator_E862 =
            ((static_cast<uint32_t>(idata[off + 4]) << 24) & 0xFF000000)    //WaveForm
            | ((static_cast<uint32_t>(idata[off + 3]) << 16) & 0x00FF0000)  //SusRel
            | ((static_cast<uint32_t>(idata[off + 2]) << 8) & 0x0000FF00)   //AtDec
            | ((static_cast<uint32_t>(idata[off + 0]) << 0) & 0x000000FF);  //AVEKM
        ins.op[slt].modulator_40 = idata[off + 1];//KSLL
    }

    if(version >= 3)
    {
        ins.ms_sound_kon  = toUint16BE(idata + 62);
        ins.ms_sound_koff = toUint16BE(idata + 64);
    }
    else
    {
        ins.ms_sound_kon = 1000;
        ins.ms_sound_koff = 500;
    }

    return true;
}

bool LoadBank(WOPL_FileReader &fr, WOPL_Bank &bnk)
{
    if(!fr.isValid())
    {
        ERR("Custom bank: Invalid data stream!");
        return false;
    }

    bnk.inst_melodic.reserve(128);
    bnk.inst_percussive.reserve(128);

    char magic[32];
    std::memset(magic, 0, 32);

    uint16_t version = 0;

    uint16_t count_melodic_banks     = 1;
    uint16_t count_percusive_banks   = 1;

    if(fr.read(magic, 1, 11) != 11)
    {
        ERR("Custom bank: Can't read magic number!");
        return false;
    }

    if(std::strncmp(magic, wopl3_magic, 11) != 0)
    {
        ERR("Custom bank: Invalid magic number!");
        return false;
    }

    uint8_t version_raw[2];
    if(fr.read(version_raw, 1, 2) != 2)
    {
        ERR("Custom bank: Can't read version!");
        return false;
    }

    version = toUint16LE(version_raw);
    if(version > wopl_latest_version)
    {
        ERR("Custom bank: Unsupported WOPL version!");
        return false;
    }

    uint8_t head[6];
    std::memset(head, 0, 6);
    if(fr.read(head, 1, 6) != 6)
    {
        ERR("Custom bank: Can't read header!");
        return false;
    }

    count_melodic_banks     = toUint16BE(head);
    count_percusive_banks   = toUint16BE(head + 2);

    if((count_melodic_banks < 1) || (count_percusive_banks < 1))
    {
        ERR("Custom bank: Too few banks in this file!");
        return false;
    }

#if 0
    /*UNUSED YET*/
    bool default_deep_vibrato   = ((head[4]>>0) & 0x01);
    bool default_deep_tremolo   = ((head[4]>>1) & 0x01);

    //5'th byte reserved for Deep-Tremolo and Deep-Vibrato flags
    m_setup.HighTremoloMode = default_deep_tremolo;
    m_setup.HighVibratoMode = default_deep_vibrato;
    //6'th byte reserved for ADLMIDI's default volume model
    m_setup.VolumeModel = (int)head[5];
#endif

#if 0
    opl.dynamic_melodic_banks.clear();
    opl.dynamic_percussion_banks.clear();
#endif

#if 0
    opl.setEmbeddedBank(m_setup.AdlBank);
#endif

    if(version >= 2)//Read bank meta-entries
    {
        for(uint16_t i = 0; i < count_melodic_banks; i++)
        {
            uint8_t bank_meta[34];
            if(fr.read(bank_meta, 1, 34) != 34)
            {
                ERR("Custom bank: Fail to read melodic bank meta-data!");
                return false;
            }
#if 0
            uint16_t bank = uint16_t(bank_meta[33]) * 256 + uint16_t(bank_meta[32]);
            size_t offset = opl.dynamic_melodic_banks.size();
            opl.dynamic_melodic_banks[bank] = offset;
            //strncpy(bankMeta.name, char_p(bank_meta), 32);
#endif
        }

        for(uint16_t i = 0; i < count_percusive_banks; i++)
        {
            uint8_t bank_meta[34];
            if(fr.read(bank_meta, 1, 34) != 34)
            {
                ERR("Custom bank: Fail to read percussion bank meta-data!");
                return false;
            }
#if 0
            uint16_t bank = uint16_t(bank_meta[33]) * 256 + uint16_t(bank_meta[32]);
            size_t offset = opl.dynamic_percussion_banks.size();
            opl.dynamic_percussion_banks[bank] = offset;
            //strncpy(bankMeta.name, char_p(bank_meta), 32);
#endif
        }
    }

    uint16_t total = 128 * count_melodic_banks;
    bool readPercussion = false;

tryAgain:
    for(uint16_t i = 0; i < total; i++)
    {
        WOPL_Inst ins;
        std::memset(&ins, 0, sizeof(WOPL_Inst));
        if(!readInstrument(fr, ins, version, readPercussion))
        {
#if 0
            opl.setEmbeddedBank(m_setup.AdlBank);
#endif
            ERR("Custom bank: Fail to read instrument!");
            return false;
        }
        ins.adlins.ms_sound_kon  = ins.ms_sound_kon;
        ins.adlins.ms_sound_koff = ins.ms_sound_koff;
#if 0
        ins.adlins.adlno1 = static_cast<uint16_t>(opl.dynamic_instruments.size() | opl.DynamicInstrumentTag);
        opl.dynamic_instruments.push_back(ins.op[0]);
#endif
        ins.adlins.adlno2 = ins.adlins.adlno1;
        if(ins.fourOps)
        {
#if 0
            ins.adlins.adlno2 = static_cast<uint16_t>(opl.dynamic_instruments.size() | opl.DynamicInstrumentTag);
            opl.dynamic_instruments.push_back(ins.op[1]);
#endif
        }
#if 0
        opl.dynamic_metainstruments.push_back(ins.adlins);
#endif

        if (!readPercussion && i < 128) {
            bnk.inst_melodic.push_back(ins);
        }
        else if (readPercussion && i < 128) {
            bnk.inst_percussive.push_back(ins);
        }
    }

    if(!readPercussion)
    {
        total = 128 * count_percusive_banks;
        readPercussion = true;
        goto tryAgain;
    }

#if 0
    opl.AdlBank = ~0u; // Use dynamic banks!
    //Percussion offset is count of instruments multipled to count of melodic banks
    opl.dynamic_percussion_offset = 128 * count_melodic_banks;
#endif

#if 0
    applySetup();
#endif

    return true;
}

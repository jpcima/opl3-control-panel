
CROSS = i686-w64-mingw32-
CXX = $(CROSS)g++
WINDRES = $(CROSS)windres
CXXFLAGS = -O2
LDFLAGS = -lwinmm -lcomdlg32 -lws2_32

### for unicode build
# CXXFLAGS += -municode -DUNICODE=1 -D_UNICODE=1
# LDFLAGS += -municode

### for static linking
LDFLAGS += -static-libgcc -static-libstdc++ -Wl,-Bstatic,--whole-archive -lwinpthread -Wl,-Bdynamic,--no-whole-archive

### for windows mode/console mode
#CXXFLAGS += -mwindows
#LDFLAGS += -mwindows

all: gui.exe

clean:
	rm -f *.exe *.o *.res

%.res: %.rc resource.h
	$(WINDRES) -O coff -o $@ $<

%.o: %.cc resource.h
	$(CXX) -c -o $@ $< $(CXXFLAGS)

gui.exe: gui.o miniwopl.o OPLSynth.o dialog.res
	$(CXX) -o $@ $^ $(LDFLAGS)

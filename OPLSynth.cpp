#include "OPLSynth.h"
#include "miniwopl.h"
//#define WOPL_VERBOSE 1

void ConvertFromWOPL(
    unsigned insno,
    patchStruct &dst, patchMapStruct *melmap, percMapStruct *percmap,
    const WOPL_Inst &src)
{
    unsigned numpairs = 1;

    dst.bOp = PATCH_1_2OP;
    switch (src.adlins.flags) {
    case WOPL_Flag_Enable4OP: dst.bOp = PATCH_1_4OP; numpairs = 2; break;
    case WOPL_Flag_Pseudo4OP: dst.bOp = PATCH_2_2OP; numpairs = 2; break;
    }

    for (unsigned pairnum = 0; pairnum < numpairs; ++pairnum) {
        unsigned block = 0b100;

        /* dst.AtA0 */
        dst.bAtB0[pairnum] = block << 2; /* block number */

        /* dst.AtB0 */
        dst.bAtC0[pairnum] =
            (0b11 << 4) /* right, left */ |
            (src.op[pairnum].feedconn & 0xf);
    }

    for (unsigned opnum = 0; opnum < 4; ++opnum) {
        const adldata &srcop = src.op[opnum/2];
        uint32_t e862 = (opnum & 1) ?
            srcop.carrier_E862 : srcop.modulator_E862;
        uint32_t v40 = (opnum & 1) ?
            srcop.carrier_40 : srcop.modulator_40;

        dst.op[opnum].bAt20 = e862 & 0xff;
        dst.op[opnum].bAt40 = v40;
        dst.op[opnum].bAt60 = (e862 >> 8) & 0xff;
        dst.op[opnum].bAt80 = (e862 >> 16) & 0xff;
        dst.op[opnum].bAtE0 = (e862 >> 24) & 0xff;
    }

    if (insno < 128 && melmap) {
        melmap->wBaseTranspose = src.op[0].finetune;

        if (dst.bOp != PATCH_1_2OP) {
            melmap->wSecondTranspose = src.op[1].finetune;
            melmap->wSecondFineTune = src.voice2_fine_tune;
        }

#if defined(WOPL_VERBOSE)
        if (src.adlins.tone)
            fprintf(stderr, "instrument %u: tone ignored (%u)\n",
                    insno, src.adlins.tone);
#endif
    }
    else if (insno < 256 && percmap) {
        percmap->bBaseNote = src.adlins.tone;

#if defined(WOPL_VERBOSE)
        if (src.op[0].finetune)
            fprintf(stderr, "instrument %u: finetune(1) ignored (%d)\n",
                    insno, src.op[0].finetune);
        if (dst.bOp != PATCH_1_2OP) {
            if (src.op[1].finetune)
                fprintf(stderr, "instrument %u: finetune(2) ignored (%d)\n",
                        insno, src.op[1].finetune);
            if (src.adlins.voice2_fine_tune)
                fprintf(stderr, "instrument %u: voice2 finetune ignored (%f)\n",
                        insno, src.adlins.voice2_fine_tune);
        }
#endif
    }
}

void PatchToSyx(Syx_Writer &wr, unsigned insno, const patchStruct &pat)
{
    wr.byte(0xf0);
    wr.byte(0x7d);
    wr.byte(0x10);  // device id
    wr.byte(0x00);  // placeholder for model id
    wr.byte(0x02);  // send

    size_t cs_start = wr.pos();
    wr.string("MaliceX ", 8);
    wr.string("OP3PATCH", 8);

    wr.byte2x4(insno);

    unsigned patchlen = sizeof(patchStruct);
    wr.byte2x4(patchlen);
    for (unsigned i = 0; i < patchlen; ++i)
        wr.byte2x4(((const uint8_t *)&pat)[i]);

    wr.checksum(cs_start, wr.pos());
    wr.byte(0xf7);
}

void MelMapToSyx(Syx_Writer &wr, unsigned insno, patchMapStruct map)
{
    wr.byte(0xf0);
    wr.byte(0x7d);
    wr.byte(0x10);  // device id
    wr.byte(0x00);  // placeholder for model id
    wr.byte(0x02);  // send

    size_t cs_start = wr.pos();
    wr.string("MaliceX ", 8);
    wr.string("OP3MLMAP", 8);

    wr.byte(insno);

    map.wBaseTranspose = htons(map.wBaseTranspose);
    map.wSecondTranspose = htons(map.wSecondTranspose);
    map.wPitchEGAmt = htons(map.wPitchEGAmt);
    map.wPitchEGTime = htons(map.wPitchEGTime);
    map.wBaseFineTune = htons(map.wBaseFineTune);
    map.wSecondFineTune = htons(map.wSecondFineTune);

    unsigned maplen = sizeof(patchMapStruct) - sizeof(patchMapStruct::bReservedPadding);
    wr.byte2x4(maplen);
    for (unsigned i = 0; i < maplen; ++i)
        wr.byte2x4(((const uint8_t *)&map)[i]);

    wr.checksum(cs_start, wr.pos());
    wr.byte(0xf7);
}

void PercMapToSyx(Syx_Writer &wr, unsigned insno, percMapStruct map)
{
    wr.byte(0xf0);
    wr.byte(0x7d);
    wr.byte(0x10);  // device id
    wr.byte(0x00);  // placeholder for model id
    wr.byte(0x02);  // send

    size_t cs_start = wr.pos();
    wr.string("MaliceX ", 8);
    wr.string("OP3PCMAP", 8);

    wr.byte(insno - 128);

    wr.byte2x4(sizeof(map));
    for (unsigned i = 0; i < sizeof(map); ++i)
        wr.byte2x4(((const uint8_t *)&map)[i]);

    wr.checksum(cs_start, wr.pos());
    wr.byte(0xf7);
}
